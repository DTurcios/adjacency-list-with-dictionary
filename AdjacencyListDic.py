# Adjacency List Program with Dictionary
# Dany Turcios
# This program creates an adjancecy list. Every vertex must have an edge.

from graphviz import Graph

# create an empty dictionary
adjLists_dict = {}
glbl_visit = 0
# got help from looking at tutorialspoint.com/python
class graph:
    def __init__(self, gdict = None):
        if gdict is None:
            gdict = []
        self.gdict = gdict
    # Gather keys of dictionary        
    def getVertices(self):
        return list(self.gdict.keys())
    
    # Adding vertex as a key
    def addVertex(self, vrtx):
        if vrtx not in self.gdict:
            self.gdict[vrtx] = []
    
    def edges(self):
        return self.findedges()
    
    def addEdge(self, edge):
        edge = set(edge)
        (vrtx1, vrtx2) = tuple(edge)
        if vrtx1 in self.gict:
            self.gdict[vrtx1].append(vrtx2)
        else:
            self.gdict[vrtx1] = [vrtx2]

    # find the list of edges    
    def findedges(self):
        edgename = []
        for vrtx in self.gdict:
            for nxtvrtx in self.gdict[vrtx]:
                if {nxtvrtx, vrtx} not in edgename:
                    edgename.append({vrtx, nxtvrtx})
        return edgename
    
    # A function used by DFS; gathered help from geeksforgeeks.com
    def DFSVisit(self,vrtx,visited):
 
        # Mark the current node as visited and print it
        visited[vrtx]= True
        global glbl_visit #increase amount of vertices the DFS searched
        glbl_visit += 1
        print(vrtx, end = " ") # print the DFS search 
 
        # Do it again for all the vertices adjacent to this vertex
        for i in self.gdict[vrtx]:
            if visited[i] == False:
                self.DFSVisit(i, visited) 
 
    # The function to do DFS. 
    def DFS(self,vrtx):
 
        # begin by marking every vertex has false
        visited = [False]*(len(self.gdict))
 
        # Recursively run until graph has been traversed
        self.DFSVisit(vrtx,visited)
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
'''  
    def _DFS(self):
        
        def DFS_Visit(self, vrtx):
            vrtx.d = time
            vrtx.color = 'grey'
            time += 1
            for u in vrtx.adj:
                if u.color == 'white':
                    u.p = vrtx
                    DFS_Visit(u)
            vrtx.f += time
            vrtx.color = 'black'
            
        for vrtx in self.gdict:
            vrtx.p = None
            vrtx.color = 'white'
            vrtx.d = -1
            vrtx.f = -1
        time = 1
        
        for vrtx in self.gdict:
            if vrtx.color == 'white':
                DFS_Visit(vrtx)   
'''  
        
    

# Functions for matrix >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
       
# Function to connect two vertices
def addEdge(u, v):
    # 'setdefault' initializes the value for that key if 
    # that key is not defined, otherwise it does nothing
    adjLists_dict.setdefault(u,[]).append(v)
    
# This function adds a 1 to a matrix if the edge exsists
def addEdgeList(c, i, j):
    c[i][j] = 1
    
# Func to display Adj. List
def displayList(adjList):
    n = len(adjList)
    for vertex in range(0,n):
        print(vertex, ":", adjList[vertex])

# Func to display Adj. Matrix
def displayMatrix(adjList):
    n = len(adjList)
    a = [i for i in range(0, n)]
    print("X : " + str(a))
    print("")
    for vertex in range(0,n):
        print(vertex, ":", adjList[vertex])
        
# DSS Class Code >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

class DSS_Obj:
    def __init__(self, value):
        self.value = value
        self.parent = None

class DSS:
    def __init__(self):
            self.data = []
        
    def add(self, value):
            self.data.append(DSS_Obj(value))
            
    def find_index(self, value):
        for (i,d) in enumerate(self.data):
            if d.value == value:
                return i
    
    def find(self,value):
        i = self.find_index(value)
        if self.data[i].parent == None:
            return (i, value)
        else:
            return self.find(self.data[self.data[i].parent].value)
        
    def show(self):
            for d in self.data:
                print(f"{d.value} has parent {d.parent}")
                
A = DSS()


# Depth First Search >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>       

#class DFS(object):
    


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

print("This program will generate an adjancency matrix,")
print("while using a dictionary. The first vertex starts at 0.")

    
# Recieve the number of vertices
totalVertex = int(input("How many vertices are there: "))
totalEdge = 0

# creates a matrix totalVertex**2
c = [[0 for j in range(totalVertex)] for i in range(totalVertex)]
   
# if there are no vertices then exit program
if(totalVertex == 0):
    print("Empty Graph")
    raise SystemExit
    
# else continue program
else:
    
    # Go through every vertex    
    for i in range(0, totalVertex):
        print("")
        print("Vertex " + str(i) + " : ")
    
        # Recieve input for number of edges
        edges = int(input("How many edges does Vertex " + str(i) + " have: "))
        totalEdge += edges
        
        # if an edge exits
        if(edges != 0):    
            
            # For every vertex get edge information
            for j in range(0 , edges):
        
                # Recieve Edge information
                edgeIn = int(input("What vertex is connected to Vertex " +str(i)+" : "))
        
                # Add value to adj. list with same key
                addEdge(i, edgeIn) #DICTIONARY
                addEdgeList(c, i, edgeIn) #ADJ_LIST
                A.add(edgeIn) #DSS
                
        # if no edges exits enter -1
        else:
            if(edges == 0):
                addEdge(i, None)


# testing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                
print("\nTotal number of edges: " + str(totalEdge))
print("Total number of vertices: " + str(totalVertex))
print("\nPrint all adjacency lists with corresponding vertex\n")
displayList(adjLists_dict)

print("\nPrint adjacency matrix for every vertex\n")
displayMatrix(c)    

g = Graph('G', filename='graph') # Taken from process.py on graphviz documentation
_g = graph(adjLists_dict) # creates an object from class graph. recieves info from adj_Lists

# copy of matrix for testing/displaying purposes
d = c
      
# display graph
# Information gathered from graphviz documentation
for i in range(totalVertex):
    
    # add node incase a disjoint graph
    g.node(str(i))
    for j in range(totalVertex):
        
        # Once edge has been added, update matrix so no duplicate
        # is added
        if(d[i][j] == 1 and d[j][i] == 1):
            g.edge(str(i),str(j))
            d[i][j] = 0
            
g.view() # display graph


print("\nVertices of the graph: ")
print(_g.getVertices())
print("Edges of the graph: ")
print(_g.edges())

temp = int(input("Start DFS from what vertex: "))
_g.DFS(temp)
#_g._DFS()
if (glbl_visit != totalVertex):
    print("\nThis set is disjoint")
else:
    print("\nThis set is not disjoint")
    
print("\n")
# Gotten help from stackoverflow to for the 'setdefault'
# Help from p-nand-q.com for disjoint sets

A.show()